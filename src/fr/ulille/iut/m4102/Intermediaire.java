package fr.ulille.iut.m4102;

import java.net.Socket;
import java.util.Arrays;

/**
 * Cette classe introduit un intermédiaire entre la classe utilisatrice
 * et l'implémentation du traitement des chaînes.
 * Au début cette classe se contente de logger ce qui se passe puis
 * elle va évoluer pour accéder au service à distance.
 * Le comportement de cette classe est totalement transparent pour la
 * classe Utilisatrice qui au final utilise les mêmes méthodes que si elle
 * appelait directement la classe AlaChaine.
 */
public class Intermediaire implements AlaChaineInterface {
    public Intermediaire() {
	// pour l'instant on accède directement au service après instanciation
    }
    
    public int nombreMots(String chaine) {
        //CALL:nombreMots:param[string,"Travail à la  chaîne"]
        String invoc = "CALL:nombreMots:param[string,\"" + chaine + "\"";
        String result =  new Client("127.0.0.1", 5300).envoyer(invoc);
        return Integer.parseInt(result.split("\"")[1]);
    }

    public String asphyxie(String chaine) throws PasDAirException {
        //CALL:nombreMots:param[string,"Travail à la  chaîne"]
        String invoc = "CALL:asphyxie:param[string,\"" + chaine + "\"]";
        String result = new Client("127.0.0.1", 5300).envoyer(invoc);
        if (result.split(":")[2].split("\\[")[0].equals("error")) {
            throw new PasDAirException(result.split("\"")[1]);
        }
        return result.split("\"")[1];
    }

    public String leetSpeak(String chaine) {
        String invoc = "CALL:leetSpeak:param[string,\"" + chaine + "\"]";
        String result = new Client("127.0.0.1", 5300).envoyer(invoc);
        return result.split("\"")[1];
    }

    public int compteChar(String chaine, char c) {
        String invoc = "CALL:compteChar:param[string,\"" + chaine + "\"]:param[char,\"" + c +"\"]";
        String result =  new Client("127.0.0.1", 5300).envoyer(invoc);
        return Integer.parseInt(result.split("\"")[1]);
    }

}
