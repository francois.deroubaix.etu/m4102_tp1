package fr.ulille.iut.m4102;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Cette classe se trouvera côté service
 * Elle recevra une requête sous forme de chaîne
 * de caractère conforme à votre protocole.
 * Elle transformera cette requête en une
 * invocation de méthode sur le service puis
 * renverra le résultat sous forme de chaîne
 * de caractères.
 */
public class AccesService extends Thread{
    private AlaChaine alc;
    private Socket client;
    public AccesService(Socket client){
        this.client = client;
	    alc = new AlaChaine();
    }
    public AccesService() {
        alc = new AlaChaine();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        traiteRequete();
    }

    public void traiteRequete() {
        try ( BufferedReader bf = new BufferedReader(new InputStreamReader(client.getInputStream()));){
            String invocation = bf.readLine();
            PrintWriter envoi = new PrintWriter(client.getOutputStream(), true);
            String method = invocation.split(":")[1];
            String result;
            switch (method){
                case "nombreMots":
                    int nbmot = alc.nombreMots(invocation.split("\"")[1]);
                    result = "RES:nombreMots:valeur[int,\"" + nbmot +"\"]";
                    envoi.println(result);
                    break;
                case "asphyxie":
                    try {
                        String withNoR = alc.asphyxie(invocation.split("\"")[1]);
                        result = "RES:asphyxie:valeur[string,\"" + withNoR +"\"]";
                        envoi.println(result);
                    }catch (Exception e){
                        result = "RES:asphyxie:error[string,\"déjà asphyxié\"]";
                        envoi.println(result);
                    }
                    break;
                case "leetSpeak":
                    String leetString = alc.leetSpeak(invocation.split("\"")[1]);
                    result = "RES:leetSpeak:valeur[string,\"" + leetString +"\"]";
                    envoi.println(result);
                    break;
                case "compteChar":
                    int nbChar = alc.compteChar(invocation.split("\"")[1],invocation.split("\"")[3].charAt(0));
                    result = "RES:leetSpeak:valeur[int,\"" + nbChar +"\"]";
                    envoi.println(result);
                    break;
                default:
                    System.out.println("la fonction n'existe pas");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String traiteInvocation(String invocation) {
        String method = invocation.split(":")[1];
        String result = "";
        switch (method){
            case "nombreMots":
                int nbmot = alc.nombreMots(invocation.split("\"")[1]);
                result = "RES:nombreMots:valeur[int,\"" + nbmot +"\"]";
                break;
            case "asphyxie":
                try {
                    String withNoR = alc.asphyxie(invocation.split("\"")[1]);
                    result = "RES:asphyxie:valeur[string,\"" + withNoR +"\"]";
                }catch (Exception e){
                    result = "RES:asphyxie:error[string,\"déjà asphyxié\"]";
                }
                break;
            case "leetSpeak":
                String leetString = alc.leetSpeak(invocation.split("\"")[1]);
                result = "RES:leetSpeak:valeur[string,\"" + leetString +"\"]";
                break;
            case "compteChar":
                int nbChar = alc.compteChar(invocation.split("\"")[1],invocation.split("\"")[3].charAt(0));
                result = "RES:leetSpeak:valeur[int,\"" + nbChar +"\"]";
                break;
            default:
                System.out.println("la fonction n'existe pas");
        }
	return result;
    }
}
